package com.example.kotlincalc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val message = intent.getStringExtra("Hasil")

        val textView = findViewById<TextView>(R.id.resultsTV).apply{
            text = message
        }
    }

}